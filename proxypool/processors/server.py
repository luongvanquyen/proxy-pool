from flask import Flask, g, request

from proxypool.schemas.proxy import Proxy
from proxypool.setting import API_HOST, API_PORT, API_THREADED, IS_DEV
from proxypool.storages.redis import RedisClient
from proxypool.utils.proxy import convert_proxy_or_proxies

__all__ = ['app']

app = Flask(__name__)
if IS_DEV:
    app.debug = True


def get_conn():
    """
    get redis client object
    :return:
    """
    if not hasattr(g, 'redis'):
        g.redis = RedisClient()
    return g.redis


@app.route('/')
def index():
    """
    get home page, you can define your own templates
    :return:
    """
    return '<h2>Welcome to Proxy Pool System</h2>'


@app.route('/random')
def get_proxy():
    """
    get a random proxy
    :return: get a random proxy
    """
    conn = get_conn()
    return conn.random().string()


@app.route('/all')
def get_proxy_all():
    """
    get a random proxy
    :return: get a random proxy
    """
    conn = get_conn()
    proxies = conn.all()
    proxies_string = ''
    if proxies:
        for proxy in proxies:
            proxies_string += str(proxy) + '\n'

    return proxies_string


@app.route('/count')
def get_count():
    """
    get the count of proxies
    :return: count, int
    """
    conn = get_conn()
    return str(conn.count())


@app.route('/add', methods=['POST'])
def add_proxy():
    """
    add the proxy to redis
    :return: success
    """
    json_data = request.get_json()
    if not json_data or 'proxies' not in json_data:
        return 'required proxies field'
    proxies = json_data['proxies']
    result = convert_proxy_or_proxies(proxies)
    conn = get_conn()
    if isinstance(result, list):
        for proxy in result:
            conn.add(proxy)
    if isinstance(result, str):
        conn.add(result)

    return 'success'


if __name__ == '__main__':
    app.run(host=API_HOST, port=API_PORT, threaded=API_THREADED)
